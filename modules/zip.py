import os
import zipfile
from modules.read import files_in_directory


def zip_files(source_dir: str, dest_dir: str) -> list[str]:
    logs = []

    for current_file in files_in_directory(source_dir):
        file_root = os.path.splitext(current_file)[0]
        zip_file_name = file_root + '.zip'
        zip_file_path = os.path.join(dest_dir, zip_file_name)
        try: 
            with zipfile.ZipFile(zip_file_path, mode='w') as zf:
                zf.write(current_file, compress_type=zipfile.ZIP_DEFLATED)

            logs.append(f'{current_file} compressed to {zip_file_name}')

        except Exception as err:
            logs.append(f"Error compressing {current_file}: {err}")
    
    return logs
