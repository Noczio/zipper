from modules.error import ZeroLogsException


def add_log(logs: list[str], filename: str):
    with open(filename, 'a') as file:
        for data in logs:
            file.write(data + '\n')


def create_log(logs: list[str], filename: str):
    try:
        if len(logs) > 0:
            add_log(logs, filename)
        else:
            raise ZeroLogsException("Zero files compressed")
    except Exception as err:
        add_log([str(err)], filename)
