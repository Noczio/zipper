import os


def files_in_directory(directory: str):
    not_wanted = ('.bat', '.zip', '.rar', '.tar', '.gz',
                  '.7zp', '.zlib', '.bz2', '.lzma', '.exe', '.py', '.log')
    
    for filename in os.listdir(directory):
        
        filepath = os.path.join(directory, filename)

        if os.path.isfile(filepath):
            # Extracting file extension
            _, file_extension = os.path.splitext(filename)
        
            if not file_extension in not_wanted:
                yield filename


def absolute_path(file_name: str, base_dir: str = os.getcwd()) -> str:
    file_path = os.path.join(base_dir, file_name)
    return os.path.normpath(file_path)
