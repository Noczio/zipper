import os
from modules.zip import zip_files
from modules.log import create_log


if __name__ == "__main__":
    source_dir = os.getcwd()
    logs_path = os.path.join(source_dir, "logs.log")

    logs = zip_files(source_dir, source_dir)
    create_log(logs, logs_path)
