# Zipper (All Files Zipper)

This repo is but a simple Python script/executable to zip all files where the .exe is located.

## Instructions

- Download the .exe from the releases.
- Place the .exe in a folder and the files that you need to compress.
- Execute the app and wait.

If by any chance you prefer to compile it by yourself, follow these steps inside a terminal with Ananconda/Miniconda and git installed:

Clone the repo
> git clone https://gitlab.com/Noczio/zipper.git

Get inside the folder
> cd .\zipper\

Create a conda env
> conda env create -f environment.yml

Activate the conda env
> conda activate File-zipper

Compile the software
> pyinstaller -n zipper -w -i zipper.ico -F app.py

## Notes

- The following files won't be compressed '.bat', '.zip', '.rar', '.tar', '.gz', '.7zp', '.zlib', '.bz2', '.lzma', '.exe', '.py' and '.log'. You're supposed to use this script with different extensions.

## Important

Because this was compiled with pyinstaller windows detects it as a virus, so it's advised to add the folder where the .exe is to an exclusion list. Yes, even if you decided it to compile it yourself.