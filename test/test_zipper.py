import os
import shutil
import tempfile
import zipfile
import unittest

from modules.zip import zip_files
from modules.read import absolute_path
from modules.log import create_log


class TestZipFiles(unittest.TestCase):

    def test_create_logs(self):
        source_dir = absolute_path('test/')
        logs = ['file1', 'file2']
        file_path = os.path.join(source_dir, "logs.log")
        create_log(logs, file_path)

        self.assertTrue(os.path.exists(file_path))
        os.remove(file_path)

    def test_zip_files(self):
        source_dir = absolute_path('test/')

        file1_path = os.path.join(source_dir, "file1.txt")
        file1_zip_path = os.path.join(source_dir, 'file1.zip')
        file2_path = os.path.join(source_dir, "file2.txt")
        file2_zip_path = os.path.join(source_dir, 'file2.zip')

        with open(file1_path, "a") as file1:
            file1.write("This is file 1.")

        with open(file2_path, "a") as file2:
            file2.write("This is file 2.")

        logs = zip_files(source_dir, source_dir)
        self.assertEqual(len(logs), 2)  # Two files should be compressed

        self.assertTrue(os.path.exists(file1_zip_path))
        self.assertTrue(os.path.exists(file2_zip_path))

        os.remove(file1_path)
        os.remove(file2_path)
        os.remove(file1_zip_path)
        os.remove(file2_zip_path)


if __name__ == '__main__':
    unittest.main()
